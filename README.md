# Begin.css
Small and modern CSS reset by Mateusz Pająk from http://aircode.pl

Begin.css style set is an effect of couple of years of working on my websites. Hope it will help someone in styling.

## Usage:

Copy or download files and add to your project.

## License:

You are free to copy and use.

*Begin styling with begin.css :)*
